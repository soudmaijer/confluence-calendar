/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.ical.action;

import com.atlassian.confluence.extra.calendar.ical.model.ICalCalendar;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.ICalendar;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.codec.binary.Base64;
import org.randombits.source.URLSource;
import org.randombits.source.StringSource;
import org.randombits.source.SourceException;
import org.randombits.source.Source;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.BufferedReader;

/**
 * Allows downloading of local iCalendars. <p/> User: david Date: Apr 23, 2006
 * Time: 12:54:28 PM
 */
public class DownloadICalCalendarAction extends AbstractICalCalendarAction {
    private static final String INVALID_SOURCE = "invalidsource";

    private static final String MISSING_CALENDAR = "missingcalendar";

    private User remoteUser;

    public DownloadICalCalendarAction() {
        super( false );
    }

    private boolean authenticateUser( HttpServletRequest req ) {
        String auth = req.getHeader( "Authorization" );

        if ( auth != null && auth.toUpperCase().startsWith( "BASIC " ) ) {
            // Get encoded user and password, comes after "BASIC "
            String userpassEncoded = auth.substring( 6 );

            try {
                // Decode it, using any base 64 decoder
                String userpassDecoded = new String( Base64
                        .decodeBase64( userpassEncoded.getBytes( "iso-8859-1" ) ) );

                // Check our user list to see if that user and password are
                // "allowed"
                String username, password;
                int split = userpassDecoded.indexOf( ':' );
                if ( split >= 0 ) {
                    username = userpassDecoded.substring( 0, split );
                    password = userpassDecoded.substring( split + 1, userpassDecoded.length() );

                    if ( userAccessor.authenticate( username, password ) ) {
                        remoteUser = userAccessor.getUser( username );
                        AuthenticatedUserThreadLocal.setUser( remoteUser );
                        return true;
                    }
                }
            } catch ( UnsupportedEncodingException e ) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean isPermitted() {
        User user = getRemoteUser();

        if ( user == null ) {
            HttpServletRequest req = ServletActionContext.getRequest();
            authenticateUser( req );
        }

        return super.isPermitted();
    }

    public User getRemoteUser() {
        if ( remoteUser != null )
            return remoteUser;

        return super.getRemoteUser();
    }

    public String downloadCalendar() throws CalendarException, UnsupportedEncodingException {
        ICalendar cal = getSubCalendar();
        if ( cal instanceof ICalCalendar ) {
            ICalCalendar iCal = ( ICalCalendar ) cal;

            // First, check that we're not going to get into an infinite loop by
            // downloading ourself.
            if ( iCal.getSource() instanceof URLSource ) {
                return INVALID_SOURCE;
            }

            HttpServletResponse resp = ServletActionContext.getResponse();
            if ( resp != null ) {
                resp.setHeader( "Content-Disposition", "attachment; filename=\"" + cal.getName() + ".ics\"" );
                resp.setContentType( "text/calendar; charset=utf-8" );
            }

            return SUCCESS;
        }

        return MISSING_CALENDAR;
    }

    public void setCid( String calendarId ) {
        setCalendarId( calendarId );
    }

    public void setScid( String subCalendarId ) {
        setSubCalendarId( subCalendarId );
    }

    /**
     * Returns the iCalendar text content as a String.
     * 
     * @return the iCalendar-format content of the current calendar.
     * @throws java.io.IOException
     *             if the source cannot be read.
     * @throws org.randombits.source.SourceException
     *             If the source cannot be read.
     */
    public String getICalendarContent() throws IOException, SourceException {
        ICalCalendar cal = ( ICalCalendar ) getSubCalendar();

        if ( cal != null ) {
            Source src = cal.getSource();
            if ( src instanceof StringSource ) {
                return ( ( StringSource ) src ).getValue();
            } else {
                StringBuffer out = new StringBuffer();

                BufferedReader in = new BufferedReader( src.openReader() );
                String line;
                while ( ( line = in.readLine() ) != null ) {
                    out.append( line ).append( "\r\n" );
                }

                return out.toString();
            }
        }
        return null;
    }

}
