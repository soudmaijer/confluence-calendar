/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import net.fortuna.ical4j.util.CompatibilityHints;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.randombits.source.StringSource;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.extra.calendar.ical.model.ICalCalendar;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.CalendarGroup;
import com.atlassian.confluence.extra.calendar.model.ICalendar;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.user.preferences.UserPreferences;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.thoughtworks.xstream.XStream;

/**
 * Central manager to handle all Calendar operations.
 */
public class CalendarManager {
    private static final int MILLIS_PER_SECOND = 1000;

    private static final int SECONDS_PER_MINUTE = 60;

    private static final int MINUTES_PER_HOUR = 60;

    private static final int MILLIS_PER_MINUTE = SECONDS_PER_MINUTE * MILLIS_PER_SECOND;

    private static final int MILLIS_PER_HOUR = MINUTES_PER_HOUR * MILLIS_PER_MINUTE;

    private static final String ETC_REGION = "Etc/";

    private static final String START_TIME_PROP = "com.atlassian.confluence.extra.calendar.startTime";

    private static final String SAVE_KEY_ROOT = "confluence.extra.calendar:";

    private static final String RELAXED_PARSING_KEY = "relaxedParsing";

    private static final String RELAXED_UNFOLDING_KEY = "relaxedUnfolding";

    private static final String RELAXED_VALIDATION_KEY = "relaxedValidation";

    private static final String OUTLOOK_COMPATIBLE_KEY = "outlookCompatible";

    private static final String NOTES_COMPATIBLE_KEY = "notesCompatible";

    public static final String FALSE = "false";

    public static final String TRUE = "true";

    private static final LocalTime START_TIME_DEFAULT = new LocalTime( 8, 0, 0, 0 );

    private static final Logger LOG = Logger.getLogger( CalendarManager.class );

    private static CalendarManager SINGLETON = new CalendarManager();

    private static Map myCalendarGroups = new HashMap();

    /**
     * Returns a singleton instance of the CalenderManager.
     * 
     * @return The CalendarManager instance.
     */
    public static CalendarManager getInstance() {
        return SINGLETON;
    }

    private ContentPropertyManager contentPropertyManager;

    private UserAccessor userAccessor;

    private XStream xStream;

    private List handlers;

    private List handlersUM;

    private CalendarHandler unknownCalendarHandler;

    private boolean relaxedParsing = false;

    private boolean relaxedValidation = false;

    private boolean relaxedUnfolding = false;

    private boolean outlookCompatible = false;

    private boolean notesCompatible = false;

    private boolean relaxedParsingDirtyFlag = true;

    private boolean relaxedValidationDirtyFlag = true;

    private boolean relaxedUnfoldingDirtyFlag = true;

    private boolean outlookCompatibleDirtyFlag = true;

    private boolean notesCompatibleDirtyFlag = true;

    private BandanaManager bandanaManager;

    public CalendarManager() {
        ContainerManager.autowireComponent( this );
        
        xStream = new XStream();
        xStream.setClassLoader( getClass().getClassLoader() );

        handlers = new java.util.LinkedList();
        handlersUM = Collections.unmodifiableList( handlers );

        unknownCalendarHandler = new UnknownCalendarHandler();
        
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_PARSING, isRelaxedParsing() );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_UNFOLDING, isRelaxedUnfolding() );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_VALIDATION, isRelaxedValidation() );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_OUTLOOK_COMPATIBILITY, isOutlookCompatible() );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_NOTES_COMPATIBILITY, isNotesCompatible() );
    }

    public void registerHandler( CalendarHandler handler ) {
        handlers.add( handler );
        // Alias the class name so that handlers in uploaded plugins will thaw
        // correctly.
        xStream.alias( handler.getCalendarClass().getName(), handler.getCalendarClass() );
    }

    public boolean deregisterHandler( CalendarHandler handler ) {
        return handlers.remove( handler );
    }

    public CalendarHandler findHandler( ICalendar calendar ) {
        if ( calendar != null )
            return findHandler( calendar.getClass() );

        return unknownCalendarHandler;
    }

    public CalendarHandler findHandler( Class calendarClass ) {
        if ( calendarClass != null ) {
            return findHandler( calendarClass.getName() );
        }
        return unknownCalendarHandler;
    }

    public CalendarHandler findHandler( String calendarClassName ) {
        if ( calendarClassName != null ) {
            Iterator i = handlers.iterator();
            CalendarHandler handler;
            Class handlerClass;

            while ( i.hasNext() ) {
                handler = ( CalendarHandler ) i.next();
                handlerClass = handler.getCalendarClass();

                if ( handlerClass != null && calendarClassName.equals( handlerClass.getName() ) )
                    return handler;
            }
        }

        return unknownCalendarHandler;
    }

    /**
     * The default handler is used when no more specific handler is available.
     * 
     * @return the default calendar handler.
     */
    public CalendarHandler getUnknownCalendarHandler() {
        return unknownCalendarHandler;
    }

    /**
     * Returns an unmodifiable list of the registered handlers. This list does
     * <b>not</b> contain the 'unknown calendar' handler.
     * 
     * @return The handler list.
     * @see #getUnknownCalendarHandler()
     */
    public List getHandlers() {
        return handlersUM;
    }

    /**
     * Loads a calendar using the passed contentObject and calendar id - if it
     * doesn't exist it will return null.
     */
    public ICalendar getCalendar( ContentEntityObject contentObject, String id ) {
        return getCalendar( contentObject, id, null, false );
    }

    /**
     * Loads a calendar using the passed contentObject and calendar id - if it
     * doesn't exist it will create it with the defaultTitle. The defaultTitle
     * can be null in all cases.
     */
    public ICalendar getCalendar( ContentEntityObject contentObject, String id, String title ) {
        return getCalendar( contentObject, id, title, true );
    }

    public ICalendar getCalendar( ContentEntityObject contentObject, String id, String title, boolean autoCreate ) {
        ICalendar calendar = loadCalendar( contentObject, id, title, autoCreate );
        if ( calendar instanceof CalendarGroup ) {
            // Check to see if we need to refresh the data in this calendar
            Iterator i = ( ( CalendarGroup ) calendar ).getChildren().iterator();
            ICalendar cal;
            while ( i.hasNext() ) {
                cal = ( ICalendar ) i.next();
                if ( cal instanceof ICalCalendar ) {
                    ICalCalendar iCal = ( ICalCalendar ) cal;
                    try {
                        iCal.readFromSource();
                    } catch ( CalendarException e ) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return calendar;
    }

    private ICalendar loadCalendar( ContentEntityObject contentObject, String id, String defaultTitle,
            boolean autoCreate ) {
        // log.debug("CalendarManager.getCalendar(" + id + ")");

        String contentKey = createContentKey( id );
        Object cacheKey = createCacheKey( contentObject, id );
        CalendarGroup calendarGroup;

        if ( myCalendarGroups.containsKey( cacheKey ) ) {
            // Get an existing calendar group
            calendarGroup = ( CalendarGroup ) myCalendarGroups.get( cacheKey );
        } else {
            // Read or create a new calendar
            String listAsXml = contentPropertyManager.getTextProperty( contentObject, contentKey );
            if ( listAsXml == null ) {
                // Do I auto create?
                if ( !autoCreate )
                    return null;
                // Create the calendar
                // log.debug("no calendar found, creating new calendar");
                calendarGroup = new CalendarGroup();
                calendarGroup.setId( id );

                if ( defaultTitle != null ) {
                    ICalCalendar defaultCalendar = new ICalCalendar();
                    defaultCalendar.setName( defaultTitle );
                    defaultCalendar.setSource( new StringSource() );
                    defaultCalendar.setColor( "blue" );
                    calendarGroup.addChild( defaultCalendar );
                }
            } else {
                calendarGroup = ( CalendarGroup ) xStream.fromXML( listAsXml );
                // log.debug("existing calendar found");
            }
            try {
                calendarGroup.init();
            } catch ( CalendarException e ) {
                LOG.error( "Error while loading the calendar calendar", e );
            }
            myCalendarGroups.put( cacheKey, calendarGroup );
        }
        return calendarGroup;
    }

    public void saveCalendar( ContentEntityObject content, String id, ICalendar calendar )
            throws CalendarException {
        if ( calendar == null )
            contentPropertyManager.setTextProperty( content, createContentKey( id ), null );
        else {
            calendar.commit();
            String listXml = xStream.toXML( calendar );
            LOG.debug( "Saving calendar: " + listXml );
            contentPropertyManager.setTextProperty( content, createContentKey( id ), listXml );
        }
    }

    /**
     * Returns the user's time zone. If the user has not yet specified one, or
     * the user is 'anonymous', the server's default timezone is returned.
     * 
     * @param user
     *            The user to check for.
     * @return The user's timezone.
     */
    public DateTimeZone getDateTimeZone( TimeZone timeZone ) {
        DateTimeZone zone = null;

        if ( timeZone != null ) {
            String id = timeZone.getID();

            if ( id != null )
                zone = DateTimeZone.forID( id );
        }

        return DateTimeUtils.getZone( zone );
    }

    public DateTimeZone getDateTimeZone( User user ) {
        return getDateTimeZone( getTimeZone( user ) );
    }

    public TimeZone getTimeZone( User user ) {
        if ( user != null ) {
            return userAccessor.getConfluenceUserPreferences( user ).getTimeZone();
        }
        return TimeZone.getDefault();
    }

    private UserPreferences getUserPreferences( User user ) {
        return new UserPreferences( userAccessor.getPropertySet( user ) );
    }

    public LocalTime getDefaultStartTime( User user ) {
        if ( user != null ) {
            UserPreferences pref = getUserPreferences( user );
            String startTimeStr = pref.getString( START_TIME_PROP );
            if ( startTimeStr != null )
                return new LocalTime( startTimeStr );
        }
        return START_TIME_DEFAULT;
    }

    public void setDefaultStartTime( User user, LocalTime startTime ) throws CalendarException {
        if ( user != null ) {
            UserPreferences prefs = getUserPreferences( user );
            try {
                prefs.setString( START_TIME_PROP, startTime.toString() );
            } catch ( AtlassianCoreException e ) {
                throw new CalendarException( getResourceBundle().getString(
                        "error.calendar.settings.savingStartTime" ), e );
            }
        }
    }

    private String createContentKey( String name ) {
        return SAVE_KEY_ROOT + name.substring( 0, Math.min( name.length(), 100 ) );
    }

    private Object createCacheKey( ContentEntityObject content, String id ) {
        // use the hash code of the content and the id as the cache key
        return new Integer( 31 * content.hashCode() + ( id != null ? id.hashCode() : 0 ) );
    }

    /**
     * Returns the sorted list of available timezone IDs.
     * 
     * @return The timezone list.
     */
    public List getTimeZones() {
        return com.atlassian.confluence.core.TimeZone.getSortedTimeZones();
        // if (zoneSet == null)
        // {
        // Set idSet = DateTimeZone.getAvailableIDs();
        // zoneSet = new java.util.TreeSet(new DateTimeZoneComparator());
        //
        // Iterator i = idSet.iterator();
        // while (i.hasNext())
        // {
        // String id = (String) i.next();
        // if (id.indexOf('/') >= 0 && (!id.startsWith(ETC_REGION) ||
        // ETC_UTC.equals(id)))
        // zoneSet.add(DateTimeZone.forID(id));
        // }
        // }
        //
        // return zoneSet;
    }

    public String getTimeZoneLabel( DateTimeZone zone ) {
        if ( zone != null ) {
            long now = DateTimeUtils.getInstantMillis( null );
            String label = zone.getID();
            if ( label.startsWith( ETC_REGION ) )
                label = zone.getShortName( now );
            else {
                label = label.replace( '_', ' ' );
                // label += " (" + getTimeZoneOffset(zone) + ")";
            }

            return label;
        }
        return null;
    }

    public String getTimeZoneOffset( DateTimeZone zone ) {
        int offset = zone.getStandardOffset( DateTimeUtils.getInstantMillis( null ) );
        StringBuffer out = new StringBuffer();

        // Direction indicator
        if ( offset < 0 ) {
            out.append( '-' );
            offset *= -1;
        } else
            out.append( '+' );

        // Hours
        int hours = offset / MILLIS_PER_HOUR;
        if ( hours < 10 )
            out.append( '0' );
        out.append( hours );
        offset -= hours * MILLIS_PER_HOUR;

        out.append( ':' );

        // Minutes
        int mins = offset / MILLIS_PER_MINUTE;
        if ( mins < 10 )
            out.append( '0' );
        out.append( mins );

        return out.toString();
    }

    public void setContentPropertyManager( ContentPropertyManager contentPropertyManager ) {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }

    public void setBandanaManager( BandanaManager bandanaManager ) {
        this.bandanaManager = bandanaManager;
    }

    /**
     * Returns the calendar resource bundle in the user's locale, or the best
     * available if the specific locale isn't supported.
     * 
     * @param user
     *            The user
     * @return The resource bundle.
     */
    public ResourceBundle getResourceBundle( User user ) {
        // TODO: Implement locale lookup once we move to Conf 2.2
        return getResourceBundle();
    }

    private ResourceBundle getResourceBundle() {
        return getResourceBundle( Locale.getDefault() );
    }

    private ResourceBundle getResourceBundle( Locale locale ) {
        return ResourceBundle.getBundle( CalendarMacro.class.getName(), locale );
    }

    public TimeZone getTimeZone( DateTimeZone timeZone ) {
        if ( timeZone != null )
            return TimeZone.getInstance( timeZone.getID() );
        return null;
    }

    public void setRelaxedParsing( boolean value ) {
        saveValue( RELAXED_PARSING_KEY, value );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_PARSING, value );
        relaxedParsing = value;
    }

    public void setRelaxedUnfolding( boolean value ) {
        saveValue( RELAXED_UNFOLDING_KEY, value );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_UNFOLDING, value );
        relaxedUnfolding = value;
    }

    public void setRelaxedValidation( boolean value ) {
        saveValue( RELAXED_VALIDATION_KEY, value );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_RELAXED_VALIDATION, value );
        relaxedValidation = value;
    }

    public void setOutlookCompatible( boolean value ) {
        saveValue( OUTLOOK_COMPATIBLE_KEY, value );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_OUTLOOK_COMPATIBILITY, value );
        outlookCompatible = value;
    }

    public void setNotesCompatible( boolean value ) {
        saveValue( NOTES_COMPATIBLE_KEY, value );
        CompatibilityHints.setHintEnabled( CompatibilityHints.KEY_NOTES_COMPATIBILITY, value );
        notesCompatible = value;
    }

    public boolean isRelaxedParsing() {
        if ( relaxedParsingDirtyFlag == true ) {
            relaxedParsing = getValue( RELAXED_PARSING_KEY );
            relaxedParsingDirtyFlag = false;
        }
        return relaxedParsing;
    }

    public boolean isNotesCompatible() {
        if ( notesCompatibleDirtyFlag == true ) {
            notesCompatible = getValue( NOTES_COMPATIBLE_KEY );
            notesCompatibleDirtyFlag = false;
        }
        return notesCompatible;
    }

    public boolean isOutlookCompatible() {
        if ( outlookCompatibleDirtyFlag == true ) {
            outlookCompatible = getValue( OUTLOOK_COMPATIBLE_KEY );
            outlookCompatibleDirtyFlag = false;
        }
        return outlookCompatible;
    }

    public boolean isRelaxedUnfolding() {
        if ( relaxedUnfoldingDirtyFlag == true ) {
            relaxedUnfolding = getValue( RELAXED_UNFOLDING_KEY );
            relaxedUnfoldingDirtyFlag = false;
        }
        return relaxedUnfolding;
    }

    public boolean isRelaxedValidation() {
        if ( relaxedValidationDirtyFlag == true ) {
            relaxedValidation = getValue( RELAXED_VALIDATION_KEY );
            relaxedValidationDirtyFlag = false;
        }
        return relaxedValidation;
    }

    /**
     * Saves a single boolean value to bandana (global context)
     * 
     * @param key
     * @param value
     */
    private void saveValue( String key, boolean value ) {
        String temp;
        if ( value == true ) {
            temp = TRUE;
        } else {
            temp = FALSE;
        }
        bandanaManager.setValue( new ConfluenceBandanaContext(), SAVE_KEY_ROOT + key, temp );
    }

    /**
     * Read a single boolean value from bandana (global context)
     * 
     * @param key
     * @return
     */
    private boolean getValue( String key ) {
        String value = ( String ) bandanaManager.getValue( new ConfluenceBandanaContext(), SAVE_KEY_ROOT + key,
                false );
        if ( ( value != null ) && ( value.compareTo( TRUE ) == 0 ) ) {
            return true;
        } else {
            return false;
        }
    }

}
