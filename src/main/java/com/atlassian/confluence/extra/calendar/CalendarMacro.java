/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.extra.calendar.display.CalendarDisplay;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.ICalendar;
import com.atlassian.confluence.extra.calendar.util.ContentPermissionWrapper;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class CalendarMacro extends BaseMacro {
    private static final Pattern ID_PATTERN = Pattern.compile( "\\w+" );

    private static final Logger log = Logger.getLogger( CalendarMacro.class );

    private static final String SUNDAY = "SU";

    private static final String MONDAY = "MO";

    private static final String TUESDAY = "TU";

    private static final String WEDNESDAY = "WE";

    private static final String THURSDAY = "TH";

    private static final String FRIDAY = "FR";

    private static final String SATURDAY = "SA";

    private CalendarManager calendarManager;

    private BootstrapManager bootstrapManager;

    private SpacePermissionManager spacePermissionManager;

    private ContentPermissionManager contentPermissionManager;

    private SpaceManager spaceManager;

    public boolean isInline() {
        return false;
    }

    public boolean hasBody() {
        return false;
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    public String execute( Map map, String string, RenderContext renderContext ) throws MacroException {
        // retrieve a reference to the body object this macro is in
        PageContext pageContext = ( PageContext ) renderContext;
        ContentEntityObject content = pageContext.getEntity();

        // now find the _group
        String id = ( String ) map.get( "id" );
        if ( id == null )
            throw new MacroException( "Please supply the id of the calendar" );
        if ( !ID_PATTERN.matcher( id ).matches() )
            throw new MacroException( "The calendar id can only contain letters or numbers" );

        String title = ( String ) map.get( "title" );
        ICalendar calendar = getCalendarManager().getCalendar( content, id, title );

        if ( title == null )
            title = "";

        // Determine if the Subscribe link should be displayed
        String suppressSubscribeLink = (String) map.get("suppressSubscribeLink");

        if(TextUtils.stringSet(suppressSubscribeLink) && suppressSubscribeLink.equals("true"))
        {
            calendar.setSuppressSubscribeLink("true");
        }
        else
        {
            calendar.setSuppressSubscribeLink(null);
        }

        if ( !title.equals( calendar.getName() ) ) {
            calendar.setName( title );
            try {
                getCalendarManager().saveCalendar( content, id, calendar );
            } catch ( CalendarException e ) {
                throw new MacroException( e );
            }
        }

        String defaultView = ( String ) map.get( "defaultView" );
        if ( defaultView == null )
            defaultView = "month";
        else if ( !"month".equals( defaultView ) && !"week".equals( defaultView ) && !"day".equals( defaultView )
                && !"event".equals( defaultView ) )
            throw new MacroException( "Invalid default view: " + defaultView );

        boolean debug = "true".equals( map.get( "debug" ) );

        Space space = spaceManager.getSpace( pageContext.getSpaceKey() );
        User user = AuthenticatedUserThreadLocal.getUser();

        boolean editPermitted = isEditPermitted( space, pageContext.getEntity(), user );

        TimeZone timeZone = CalendarManager.getInstance().getTimeZone( user );
        DateTimeZone dateTimeZone = CalendarManager.getInstance().getDateTimeZone( timeZone );

        LocalDate today = new LocalDate( dateTimeZone );

        String contextPath = renderContext.getSiteRoot();
        if ( contextPath == null )
            contextPath = bootstrapManager.getWebAppContextPath();

        int firstDay = convertDay( ( String ) map.get( "firstDay" ) );

        // setup our model for display of the event calendar
        CalendarDisplay calendarDisplay = new CalendarDisplay( calendar, today, firstDay, timeZone, 20, Period
                .years( 1 ) );

        // Get resource bundle
        ResourceBundle resources = CalendarManager.getInstance().getResourceBundle( user );

        // now create a simple velocity context and render a template for the
        // output
        Map contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put( "calendar", calendar );
        contextMap.put( "content", content );
        contextMap.put( "calendarManager", getCalendarManager() );
        contextMap.put( "calendarDisplay", calendarDisplay );
        contextMap.put( "contextPath", contextPath );
        contextMap.put( "debug", ( debug ) ? Boolean.TRUE : Boolean.FALSE );
        contextMap.put( "spaceKey", pageContext.getSpaceKey() );
        contextMap.put( "editPermitted", editPermitted ? Boolean.TRUE : Boolean.FALSE );
        contextMap.put( "defaultView", defaultView );
        contextMap.put( "firstDay", new Integer( firstDay ) );
        contextMap.put( "resources", resources );
        contextMap.put( "remoteUser", user );

        try {
            return VelocityUtils.getRenderedTemplate( "templates/extra/calendar/calendar.vm", contextMap );
        } catch ( Exception e ) {
            log.error( "Error while trying to display calendar!", e );
            throw new MacroException( e.getMessage() );
        }
    }

    /**
     * Converts the specified day name string to a number.
     * 
     * @param dayName
     *            The name of the day in English.
     * @return The integer value for the day (Monday = 1, Sunday = 7)
     * @throws MacroException
     *             if the day is unrecognised.
     */
    private int convertDay( String dayName ) throws MacroException {
        if ( dayName != null ) {
            dayName = dayName.toUpperCase();
            if ( dayName.startsWith( SUNDAY ) )
                return DateTimeConstants.SUNDAY;
            else if ( dayName.startsWith( MONDAY ) )
                return DateTimeConstants.MONDAY;
            else if ( dayName.startsWith( TUESDAY ) )
                return DateTimeConstants.TUESDAY;
            else if ( dayName.startsWith( WEDNESDAY ) )
                return DateTimeConstants.WEDNESDAY;
            else if ( dayName.startsWith( THURSDAY ) )
                return DateTimeConstants.THURSDAY;
            else if ( dayName.startsWith( FRIDAY ) )
                return DateTimeConstants.FRIDAY;
            else if ( dayName.startsWith( SATURDAY ) )
                return DateTimeConstants.SATURDAY;
            else
                throw new MacroException( "Unsupported day value: " + dayName );
        }
        return DateTimeConstants.MONDAY;
    }

    protected List getEditPermissionTypes() {
        List permissionTypes = new ArrayList();

        permissionTypes.add( SpacePermission.USE_CONFLUENCE_PERMISSION );
        permissionTypes.add( SpacePermission.VIEWSPACE_PERMISSION );
        permissionTypes.add( SpacePermission.CREATEEDIT_PAGE_PERMISSION );

        return permissionTypes;
    }

    public boolean isEditPermitted( Space space, ContentEntityObject content, User user ) {
        if ( GeneralUtil.isSuperUser( user ) )
            return true;

        return spacePermissionManager.hasPermission( getEditPermissionTypes(), space, user )
                && hasEditPagePermission( content, user );
    }

    protected boolean hasEditPagePermission( ContentEntityObject content, User user ) {
        if ( content instanceof Page ) {
            final ContentPermissionWrapper contentPermissionWrapper = ( ContentPermissionWrapper ) ContainerManager
                    .getInstance().getContainerContext().getComponent( "contentPermissionWrapper" );
            return contentPermissionManager.hasContentLevelPermission( user, ( String ) contentPermissionWrapper
                    .getStaticFieldValue( "EDIT_PERMISSION" ), content );
        }
        return true;
    }

    protected CalendarManager getCalendarManager() {
        if ( calendarManager == null )
            calendarManager = CalendarManager.getInstance();

        return calendarManager;
    }

    public void setBootstrapManager( BootstrapManager bootstrapManager ) {
        this.bootstrapManager = bootstrapManager;
    }

    public void setSpacePermissionManager( SpacePermissionManager spacePermissionManager ) {
        this.spacePermissionManager = spacePermissionManager;
    }

    public void setContentPermissionManager( ContentPermissionManager contentPermissionManager ) {
        this.contentPermissionManager = contentPermissionManager;
    }

    public void setSpaceManager( SpaceManager spaceManager ) {
        this.spaceManager = spaceManager;
    }
}
