/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.atlassian.confluence.extra.calendar.util;

import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;

import java.util.Comparator;

import com.atlassian.confluence.extra.calendar.CalendarManager;

/**
 * Compares two timezones.
 * 
 * <p>
 * <b>Note: this comparator imposes orderings that are inconsistent with equals.</b>
 * </p>
 * 
 * @author David Peterson
 */
public class DateTimeZoneComparator implements Comparator {
    private long millis = DateTimeUtils.getInstantMillis( null );

    public DateTimeZoneComparator() {
    }

    /**
     * Constructs a comparator comparing two {@link DateTimeZone}s at the
     * specified number of milliseconds since midnight Jan 01, 1970, UTC.
     * 
     * @param millis
     *            The milliseconds since midnight Jan 01, 1970, UTC.
     */
    public DateTimeZoneComparator( long millis ) {
        this.millis = millis;
    }

    /**
     * Compares two {@link DateTimeZone}s for order, based on their offset from
     * UMT and then their human-readable name.
     * 
     * @param o1
     *            the first object to be compared.
     * @param o2
     *            the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the first
     *         argument is less than, equal to, or greater than the second.
     * @throws ClassCastException
     *             if the arguments' types prevent them from being compared by
     *             this Comparator.
     */
    public int compare( Object o1, Object o2 ) {
        DateTimeZone z1 = ( DateTimeZone ) o1;
        DateTimeZone z2 = ( DateTimeZone ) o2;

        if ( z1 == z2 || z1.equals( z2 ) )
            return 0;

        // First, filter by offset today.
        int offset1 = z1.getStandardOffset( millis );
        int offset2 = z2.getStandardOffset( millis );
        if ( offset1 < offset2 )
            return -1;
        else if ( offset1 > offset2 )
            return 1;
        else {
            String label1 = CalendarManager.getInstance().getTimeZoneLabel( z1 );
            String label2 = CalendarManager.getInstance().getTimeZoneLabel( z2 );
            return label1.compareTo( label2 );
        }
    }
}
