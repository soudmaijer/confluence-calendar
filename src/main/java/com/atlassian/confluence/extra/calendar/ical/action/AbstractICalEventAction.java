/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.ical.action;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.calendar.action.AbstractEventAction;
import com.atlassian.confluence.extra.calendar.ical.model.EventId;
import com.atlassian.confluence.extra.calendar.ical.model.ICalEvent;
import com.atlassian.confluence.extra.calendar.ical.model.ICalOrganizer;
import com.atlassian.confluence.extra.calendar.CalendarManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkRenderer;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.user.User;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.LocalTime;

import java.net.URI;

/**
 * Provides support for ICalEvent actions.
 */
public class AbstractICalEventAction extends AbstractEventAction {
    protected static final Logger log = Logger.getLogger( AbstractICalEventAction.class );

    protected static final String CONF_LINK = "CONF-LINK";

    protected String eventDescription;

    protected String organizerName;

    protected String link;

    protected LinkResolver linkResolver;

    private LinkRenderer linkRenderer;

    public AbstractICalEventAction( boolean requireEditPermission ) {
        super( requireEditPermission );
    }

    /**
     * Set Event Details Fields in GUI 
     */
    public String execute() {
    	ICalEvent event = null;
    
        
        if (isAllOccurrences() && isEditMode() && !isOverrideEvent()) {
        	//Edit AllOccurrences so show the base event
        	event = ( ICalEvent ) getBaseEvent();
        }
        else {
        	event = ( ICalEvent ) getEvent();
        }

        if ( event == null ) {
            User user = getRemoteUser();
            if ( user != null )
                setOrganizerName( user.getName() );

            LocalDateTime start = getStartDate();
            if ( start == null )
                start = new LocalDateTime( getEventDateTimeZone() );

            LocalTime startTime = CalendarManager.getInstance().getDefaultStartTime( user );
            if ( startTime == null )
                startTime = new LocalTime( 0, 0, 0 );

            start = start.withTime( startTime.getHourOfDay(), startTime.getMinuteOfHour(), 0, 0 );

            setStartDate( start );
            setEndDate( start.plus( Period.hours( 1 ) ) );
            setEventDateTimeZone( CalendarManager.getInstance().getDateTimeZone( user ) );
            
        } else {
            setEventSummary( event.getSummary() );
            setEventLocation( event.getLocation() );

            setEventDescription( event.getDescription() );

            setOrganizerName( null );
            if ( event.getOrganizer() != null ) {
                String username = event.getOrganizer().getUsername();
                if ( username == null )
                    username = event.getOrganizer().getName();
                User user = userAccessor.getUser( username );

                if ( user != null )
                    setOrganizerName( user.getName() );
            }
            
            setRepeat(event.getRepeat());//set type of recurrence
            
            setAllDay( event.isAllDay() );

            setStartDate( event.getStartDate() );

            LocalDateTime endDate = event.getEndDate();
            if ( endDate != null && isAllDay() ) // subtract a day from the
                                                    // end date
            	endDate = endDate.minus( Period.days( 1 ) );

            setEndDate( endDate );
            
            setEventDateTimeZone( event.getTimeZone() );
            
            
            String confLink = event.getStringProperty( CONF_LINK, null );
            if ( confLink == null ) {
                URI url = event.getUrl();
                if ( url != null )
                    confLink = url.getPath();
            }
            setLink( confLink );
        }

        return INPUT;
    }

    protected void setEventId( EventId eventId ) {
        setEventId( eventId.toString() );
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription( String eventDescription ) {
        this.eventDescription = eventDescription;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public void setOrganizerName( String organizerName ) {
        this.organizerName = organizerName;
    }

    public String getLink() {
        return link;
    }

    public void setLink( String link ) {
        this.link = link;
    }

    public String getLinkHtml() {
        ICalEvent event = ( ICalEvent ) getEvent();

        if ( event != null ) {
            String evtLink = event.getStringProperty( CONF_LINK, null );

            if ( evtLink != null ) {
                ContentEntityObject content = getContent();

                if ( content != null ) {
                    PageContext ctx = content.toPageContext();
                    ctx.setSiteRoot( getContextPath() );
                    ctx.setImagePath( getContextPath() + "/images" );

                    Link rlink = linkResolver.createLink( ctx, evtLink );
                    if ( rlink != null )
                        return linkRenderer.renderLink( rlink, ctx );
                }
            }

            URI uri = event.getUrl();

            if ( uri != null ) {
                String path = uri.getPath();

                String disp = path;
                if ( disp.length() > 25 )
                    disp = disp.substring( 0, 10 ) + "..." + disp.substring( disp.length() - 10 );

                return "<a href=\"" + path + "\" title=\"" + path + "\">" + disp + "</a>";
            }
        }

        return null;
    }

    public String getOrganizerHtml( ICalOrganizer organizer ) {
        StringBuffer out = new StringBuffer();

        if ( organizer != null ) {
            String username = organizer.getUsername();
            if ( username == null ) // The name was used to store usernames
                                    // previously. Check as backup.
                username = organizer.getName();

            User user = userAccessor.getUser( username );
            if ( user != null ) {
                out.append( "<a href='" ).append( getContextPath() ).append( "/display/~" ).append(
                        GeneralUtil.doubleUrlEncode( user.getName() ) ).append( "'>" );
                if ( TextUtils.stringSet( user.getFullName() ) )
                    out.append( GeneralUtil.htmlEncode( user.getFullName() ) );
                else
                    out.append( GeneralUtil.htmlEncode( user.getName() ) );

                out.append( "</a>" );
            } else {
                String email = organizer.getEmail();
                if ( email != null ) {
                    out.append( "<a href='mailto:" ).append( GeneralUtil.urlEncode( email ) ).append( "'>" );

                    if ( TextUtils.stringSet( organizer.getName() ) )
                        out.append( organizer.getName() );
                    else
                        out.append( email );

                    out.append( "</a>" );
                } else
                    out.append( organizer.getName() );
            }
        }

        return out.toString();
    }

    public void setLinkResolver( LinkResolver linkResolver ) {
        this.linkResolver = linkResolver;
    }

    public void setLinkRenderer( LinkRenderer linkRenderer ) {
        this.linkRenderer = linkRenderer;
    }
    
    /* 
     * is this an override Event?
     */
    public boolean isOverrideEvent() {
    	String strEventId = getEventId();
    	if (strEventId == null || strEventId.trim().length() == 0 ) {
    		return false;
    	} else {
    		EventId eventId = new EventId(strEventId);
    		return  (eventId.getOverrideDate() != null ? true : false);
    	}
    }

}
