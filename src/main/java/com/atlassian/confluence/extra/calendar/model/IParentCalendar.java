/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.model;

import java.util.List;

/**
 * A parent calendar is one which can contain other calendars inside it.
 */
public interface IParentCalendar extends ICalendar {
    /**
     * An unmodifiable list of the displayable child calendars. If the parent
     * has no children, an empty List will be returned.
     * 
     * @return The list of child calendars.
     */
    List getChildren();

    /**
     * An unmodifiable list of all the descendent of this parent calendar. If
     * the parent has no descendents, an empty List will be returned.
     * 
     * @return The list of descendents.
     */
    List getDescendents();

    /**
     * Finds the direct child with the specified id, if it exists.
     * 
     * @param id
     *            The unique ID of the event.
     * @return The event, or <code>null</code> if not found.
     */
    ICalendar getChild( String id );

    /**
     * Finds the decendent child with the specified it, searching though any
     * children which also implement IParentCalendar.
     * 
     * @param id
     *            The unique id of the event.
     * @return The event, or <code>null</code> if not found.
     */
    ICalendar getDescendent( String id );

    /**
     * Removes the calendar if it is a descendent of this calendar.
     * 
     * @param calendar
     *            The calendar to remove.
     * @return <code>true</code> if the calendar was found and removed.
     */
    boolean removeDescendent( ICalendar calendar );
}
