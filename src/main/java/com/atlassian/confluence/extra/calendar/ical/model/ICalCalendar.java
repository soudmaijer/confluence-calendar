/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.ical.model;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentFactory;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.PropertyFactoryImpl;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.RecurrenceId;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.model.property.XProperty;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.randombits.source.Source;
import org.randombits.source.SourceException;
import org.randombits.source.confluence.AttachmentSource;

import com.atlassian.confluence.extra.calendar.model.ACalendar;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.IEvent;

/**
 * This is a calendar implementation that supports the iCalendar (RFC2445)
 * standard. It uses the <a href="http://ical4j.sourceforge.net/">iCal4j</a>
 * library.
 */
public class ICalCalendar extends ACalendar {
    private static final String DEFAULT_TIMEZONE = "Australia/Brisbane";

    private static final String CALENDAR_NAME = Property.EXPERIMENTAL_PREFIX + "WR-CALNAME";

    private static final String CALENDAR_DESCRIPTION = Property.EXPERIMENTAL_PREFIX + "WR-CALDESC";

    private static final String MAILTO = "mailto:";

    // This defines the period after which calendars are reread
    // from their source (in milliseconds)
    private static final long REREAD_THRESHOLD = 30 * 1000;

    private Source source;

    private long lastSourceRead = 0;

    private transient Calendar _calendar;

    private transient Map _baseDetailMap;

    private transient Logger log = Logger.getLogger( ICalCalendar.class );

    public ICalCalendar() {
    }

    public boolean isReadOnly() {
        return getErrors() == null && !source.canWrite();
    }

    /**
     * An unmodifiable list of the events in this calendar. Does <b>not</b>
     * include recurrence overrides.
     * 
     * @return the list of IEvents.
     * @throws com.atlassian.confluence.extra.calendar.model.CalendarException
     *             if there is a problem with the calendar.
     */
    private Map getBaseDetails() throws CalendarException {
        initDetails();

        return _baseDetailMap;
    }

    private void initDetails() throws CalendarException {
        if ( _baseDetailMap == null ) {
            _baseDetailMap = new java.util.HashMap();

            if ( getCalendar() != null ) {
                List overrideEvents = new java.util.ArrayList();

                Iterator i = getCalendar().getComponents().iterator();
                // First, go through all the events and create them as either
                // override or base events.
                while ( i.hasNext() ) {
                    Component c = ( Component ) i.next();
                    if ( c instanceof VEvent ) {
                        VEvent evt = ( VEvent ) c;

                        RecurrenceId recurrenceId = ( RecurrenceId ) evt.getProperties().getProperty(
                                Property.RECURRENCE_ID );

                        if ( recurrenceId != null )
                            overrideEvents.add( new OverrideEventDetails( this, evt ) );
                        else {
                            BaseEventDetails bevt = new BaseEventDetails( this, evt );
                            _baseDetailMap.put( bevt.getUid(), bevt );
                        }
                    }
                }

                // Then, go back through the override events and assign them to
                // their base event.
                i = overrideEvents.iterator();
                while ( i.hasNext() ) {
                    OverrideEventDetails evt = ( OverrideEventDetails ) i.next();
                    BaseEventDetails bevt = ( BaseEventDetails ) _baseDetailMap.get( evt.getUid() );
                    if ( bevt != null ) {
                        bevt.addOverride( evt );
                    } else {
                        log.error( "Unable to find base event for an override: " + evt.getUid() );
                    }
                }
            }
        }
    }

    public void addBaseDetails( BaseEventDetails eventDetails ) throws CalendarException {
        checkReadOnly();

        if ( eventDetails.getCalendar() instanceof ICalCalendar )
            ( ( ICalCalendar ) eventDetails.getCalendar() ).removeBaseDetails( eventDetails );

        if ( _baseDetailMap != null )
            _baseDetailMap.put( eventDetails.getUid(), eventDetails );

        getCalendar().getComponents().add( eventDetails.getEventComponent() );
        eventDetails.setCalendar( this );
    }

    /**
     * This method removes entries from the calendar.
     * 
     * @param eventDetails
     * @return
     * @throws CalendarException
     */
    public boolean removeBaseDetails( BaseEventDetails eventDetails ) throws CalendarException {
        checkReadOnly();

        boolean success = false;

        if ( eventDetails.getCalendar() == this ) {
            if ( _baseDetailMap != null ) {
            	removeAllOverrideDetails( eventDetails ) ;
                Map tempBaseDetailMap = new java.util.HashMap();
                // Iterate through the map of events and retain the
                // ones that will not be deleted.
                for ( Iterator i = _baseDetailMap.entrySet().iterator(); i.hasNext(); ) {
                    Map.Entry pair = ( Map.Entry ) i.next();
                    String key = ( String ) pair.getKey();
                    if ( key.compareTo( eventDetails.getUid() ) != 0 ) {
                        tempBaseDetailMap.put( key, pair.getValue() );
                    }
                }
                _baseDetailMap.clear();
                _baseDetailMap = tempBaseDetailMap;
            }
            success = getCalendar().getComponents().remove( eventDetails.getEventComponent() );
           
            
            
        }
        return success;
    }

    public void addOverrideDetails( OverrideEventDetails eventDetails ) throws CalendarException {
        checkReadOnly();

        if ( eventDetails.getCalendar() instanceof ICalCalendar )
            ( ( ICalCalendar ) eventDetails.getCalendar() ).removeOverrideDetails( eventDetails );

        if ( _baseDetailMap != null ) {
        	BaseEventDetails bevt = ( BaseEventDetails ) _baseDetailMap.get( eventDetails.getUid() );
            if ( bevt != null ) {
                bevt.addOverride( eventDetails );
            } else {
                log.error( "Unable to find base event for an override: " + eventDetails.getUid() );
            }
        }

        getCalendar().getComponents().add( eventDetails.getEventComponent() );
        eventDetails.setCalendar( this );
    }

    /**
     * This method removes one override detail from the calendar.
     * 
     * @param eventDetails
     * @return
     * @throws CalendarException
     */
    public boolean removeOverrideDetails( OverrideEventDetails eventDetails ) throws CalendarException {
        checkReadOnly();

        boolean success = false;

        if ( eventDetails.getCalendar() == this ) {
            if ( _baseDetailMap != null ) {
            	BaseEventDetails bevt = ( BaseEventDetails ) _baseDetailMap.get( eventDetails.getUid() );
                if ( bevt != null ) {
                	bevt.removeOverride(eventDetails);
                    success = getCalendar().getComponents().remove( eventDetails.getEventComponent() );
                } else {
                    log.error( "Unable to find base event for an override: " + eventDetails.getUid() );
                }
            }
        }
        return success;
    }

    /**
     * This method removes all Override Details from a baseDetail of a calendar
     * 
     * @param eventDetails
     * @return
     * @throws CalendarException
     */
    public boolean removeAllOverrideDetails( BaseEventDetails eventDetails ) throws CalendarException {
        checkReadOnly();
        
		boolean error=false;
        
        if ( eventDetails.getCalendar() == this ) {
            if ( _baseDetailMap != null ) {
            	BaseEventDetails bevt = ( BaseEventDetails ) _baseDetailMap.get( eventDetails.getUid() );
                if ( bevt != null ) {
                	java.util.ArrayList overrideEvents = (java.util.ArrayList) eventDetails.getOverrideEvents();
                	if (overrideEvents != null && overrideEvents.size()>0) {
                		for (int i =0;i<overrideEvents.size();i++) {
                			if (!removeOverrideDetails( (OverrideEventDetails) overrideEvents.get(i) )) {
                				error = true;
                			}
                		}
                	}
                }
            }
        }
        return error == false;
    }
    
    public void init() throws CalendarException {
        getCalendar();
    }

    public Calendar getCalendar() throws CalendarException {
        if ( _calendar == null ) {
            if ( ( source != null ) && ( source.canRead() ) ) {
                readFromSource();
            } else // create a new one from scratch.
            {
                _calendar = new Calendar();
                _calendar.getProperties().add(
                        new ProdId( "-//Atlassian Software Systems//Confluence Calendar Plugin//EN" ) );
                _calendar.getProperties().add( Version.VERSION_2_0 );
                _calendar.getProperties().add( CalScale.GREGORIAN );
            }
        }

        return _calendar;
    }

    /**
     * This method reads the contents of the specified reader as a String and
     * cleans up any errors which have been introduced be previous versions of
     * this plugin...
     * 
     * @param reader
     *            The reader to clean
     * @return A new Reader with clean content.
     * @throws java.io.IOException
     *             if there is an IO problem.
     */
    private Reader cleanReader( Reader reader ) throws IOException {
        StringBuffer buff = new StringBuffer();
        LineNumberReader in = new LineNumberReader( reader );
        String line;

        while ( ( line = in.readLine() ) != null ) {
            buff.append( cleanLine( line ) ).append( "\r\n" );
        }

        return new StringReader( buff.toString() );
    }

    /**
     * Cleans up the line of Calendar code.
     * 
     * @param line
     *            The line to clean
     * @return The cleaned line.
     */
    private String cleanLine( String line ) {
        if ( line.startsWith( "ORGANIZER;" ) && line.endsWith( MAILTO ) )
            return line.substring( 0, line.length() - MAILTO.length() );

        return line;
    }

    private Component createTempComponentIfEmpty( Calendar calendar ) {
        if ( calendar.getComponents().isEmpty() ) {
            PropertyList list = new PropertyList();
            Property uid = PropertyFactoryImpl.getInstance().createProperty( Property.UID );
            list.add( uid );
            Property dtStamp = PropertyFactoryImpl.getInstance().createProperty( Property.DTSTAMP );
            list.add( dtStamp );
            Component temp = ComponentFactory.getInstance().createComponent( Component.VEVENT, list );

            calendar.getComponents().add( temp );
            return temp;
        }
        return null;
    }

    public void commit() throws CalendarException {
        if ( source.canWrite() ) {
            Calendar calendar = getCalendar();

            if ( calendar != null ) {
                Component temp = null;
                // The the current name and description and stick 'em in.
                setCalendarName( calendar, getName() );
                setCalendarDescription( calendar, getDescription() );

                initDetails();
                resetTimeZones( calendar );

                CalendarOutputter out = new CalendarOutputter();
                try {
                    temp = createTempComponentIfEmpty( calendar );
                    out.output( calendar, source.openWriter() );
                } catch ( IOException e ) {
                    throw new CalendarException( e );
                } catch ( ValidationException e ) {
                    throw new CalendarException( e );
                } catch ( SourceException e ) {
                    throw new CalendarException( e );
                } finally {
                    if ( temp != null ) {
                        calendar.getComponents().remove( temp );
                    }
                }
            }
        }
    }

    /**
     * This method resets the timezones to include all and only those which are
     * used by events in the calendar.
     * 
     * @param calendar
     *            the calendar to reset.
     */
    private void resetTimeZones( Calendar calendar ) {
        // Clear existing timezones...
        ComponentList timeZones = calendar.getComponents( VTimeZone.VTIMEZONE );
        calendar.getComponents().removeAll( timeZones );

        // Add event timezones...
        if ( _baseDetailMap != null && _baseDetailMap.size() > 0 ) {
            Set addedTimeZones = new java.util.HashSet();
            Iterator i = _baseDetailMap.values().iterator();

            while ( i.hasNext() ) {
                BaseEventDetails details = ( BaseEventDetails ) i.next();
                if ( details != null && details.getEventComponent() != null
                        && details.getEventComponent().getStartDate() != null ) {
                    Date date = details.getEventComponent().getStartDate().getDate();
                    processTimeZone( date, addedTimeZones, calendar );
                    date = details.getEventComponent().getEndDate().getDate();
                    processTimeZone( date, addedTimeZones, calendar );

                    // CAL-124: Fix for non-UTC dtstamp timezones.
                    details.getEventComponent().getDateStamp().getDateTime().setUtc( true );
                }
            }
        } else if ( calendar.getComponents().size() == 0 ) {
            // Add the server timezone by default, since all calendars must have
            // at least
            // one component to be valid. This will never actually be used since
            // it's cleared
            // when saving a calendar with any actual events.
            TimeZone timeZone = ICalUtil.toICalTimeZone( DateTimeZone.forID( DEFAULT_TIMEZONE ) );
            calendar.getComponents().add( timeZone.getVTimeZone() );
        }
    }

    private void processTimeZone( Date date, Set addedTimeZones, Calendar calendar ) {
        if ( date instanceof DateTime ) {
            DateTime dateTime = ( DateTime ) date;
            TimeZone zone = dateTime.getTimeZone();
            if ( zone != null && !addedTimeZones.contains( zone ) ) // Add it.
            {
                calendar.getComponents().add( zone.getVTimeZone() );
                addedTimeZones.add( zone );
            }
        }
    }

    private void setCalendarName( Calendar calendar, String name ) {

        XProperty nameProp = ( XProperty ) calendar.getProperties().getProperty( CALENDAR_NAME );
        if ( nameProp == null ) {
            nameProp = new XProperty( CALENDAR_NAME, name );
            nameProp.getParameters().add( Value.TEXT );
            calendar.getProperties().add( nameProp );
        } else {
            nameProp.setValue( name );
        }
    }

    private void setCalendarDescription( Calendar calendar, String description ) {
        XProperty descProp = ( XProperty ) calendar.getProperties().getProperty( CALENDAR_DESCRIPTION );
        if ( descProp == null ) {
            descProp = new XProperty( CALENDAR_DESCRIPTION, description );
            descProp.getParameters().add( Value.TEXT );
            calendar.getProperties().add( descProp );
        } else {
            descProp.setValue( description );
        }
    }

    /**
     * Read the calendar data in from the source
     * 
     */
    public void readFromSource() throws CalendarException {
        if ( ( source != null ) && ( source.canRead() ) ) {
            // The calendar already exists, check to see if we need to re-source
            // the content.
            long now = System.currentTimeMillis();
            long age = now - lastSourceRead;
            if ( age > REREAD_THRESHOLD ) {
                Reader attachmentReader = null;
                CalendarBuilder builder = new CalendarBuilder();
                try {
                    clearErrors();
                    // create a new attachment source so it has the latest
                    // content entity object - this is hacky, but the whole
                    // calendar
                    // caching scheme needs to be revisited so this is a quick
                    // fix - rw
                    if ( source instanceof AttachmentSource ) {
                        AttachmentSource originalSource = ( AttachmentSource ) source;
                        AttachmentSource newSource = new AttachmentSource();
                        newSource.setContentId( originalSource.getContentId() );
                        newSource.setFileName( originalSource.getFileName() );
                        source = newSource;
                    }
                    attachmentReader = source.openReader();
                    _calendar = builder.build( cleanReader( attachmentReader ) );
                    _baseDetailMap = null;
                } catch ( IOException e ) {
                    addError( "Unable to access the calendar: " + e.getMessage() );
                    throw new CalendarException( e );
                } catch ( ParserException e ) {
                    addError( "Unable to read the calendar (try setting the 'relaxed parsing mode' in the calendar administration options): "
                            + e.getMessage() );
                    throw new CalendarException( e );
                } catch ( SourceException e ) {
                    addError( "Unable to access the calendar: " + e.getMessage() );
                    throw new CalendarException( e );
                } catch ( UnsupportedOperationException e ) {
                    addError( "Unsupported operation. Please contact your administrator." + e.getMessage() );
                    throw new CalendarException( e );
                } catch ( Exception e ) {
                    addError( "An unexpected problem occurred. Please contact your administrator."
                            + e.getMessage() );
                    throw new CalendarException( e );
                } finally {
                    if ( attachmentReader != null ) {
                        try {
                            attachmentReader.close();
                        } catch ( IOException e ) {
                            addError( "Unable to finalise reading the calendar: " + e.getMessage() );
                        }
                    }
                }
                lastSourceRead = now;
            }
        }
    }

    public IEvent findEvent( String id ) throws CalendarException {
        return findEvent( id, false );
    }

    /**
     * return the base event details for this event id
     */
    public IEvent findBaseEvent( String id ) throws CalendarException {
        return findEvent( id, true );
    }

    /**
     * 
     * @param id
     * @param baseEventOnly true - only return the base event
     * @return
     * @throws CalendarException
     */
    public IEvent findEvent( String id, boolean baseEventOnly ) throws CalendarException {
        EventId eventId = new EventId( id );

        BaseEventDetails baseDetails = ( BaseEventDetails ) getBaseDetails().get( eventId.getUid() );

        if ( baseDetails != null ) {
            if ( eventId.getOverrideDate() != null && !baseEventOnly ) {
                OverrideEventDetails overrideDetails = baseDetails.getOverrideDetails( eventId.getOverrideDate() );
                if ( overrideDetails != null )
                    return new ICalEvent( overrideDetails, eventId.getInstanceDate(), eventId.getTimeZone(),
                            overrideDetails.getStartOffset() );
                else
                    return null;
            } else {
            	if (!baseEventOnly)
            		return new ICalEvent( baseDetails, eventId.getInstanceDate(), eventId.getTimeZone() );
            	else 
            		return new ICalEvent( baseDetails, baseDetails.getStartDate(), baseDetails.getTimeZone() );
            }
        }

        return null;
    }


    /**
     * Returns a list of the events which occur in the specified interval.
     * Events that only have part of their duration in the interval <b>are</b>
     * included. The events are in an unspecified order.
     * 
     * @param interval
     *            The time interval to get occurrences for.
     * @param targetTimeZone
     *            The timezone the interval applies to.
     * @return The List of IEvents.
     */
    public List findEvents( Interval interval, DateTimeZone targetTimeZone ) throws CalendarException {
        Iterator i = getBaseDetails().values().iterator();
        List events = new java.util.ArrayList();

        while ( i.hasNext() ) {
            events.addAll( ( ( EventDetails ) i.next() ).getEvents( interval, targetTimeZone ) );
        }

        return events;
    }

    public Source getSource() {
        return source;
    }

    public void setSource( Source source ) {
        this.source = source;
    }
}
