/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar;

import com.atlassian.confluence.extra.calendar.model.ICalendar;

/**
 * This interface allows for custom calendar implementation to be supported.
 * Implementations can then register themselves through the
 * {@link CalendarManager#registerHandler(CalendarHandler)} method.
 * 
 * @see CalendarManager
 */
public interface CalendarHandler {
    public static final int VIEW_CALENDAR = 1;

    public static final int ADD_CALENDAR = 2;

    public static final int EDIT_CALENDAR = 3;

    public static final int VIEW_EVENT = 10;

    public static final int ADD_EVENT = 11;

    public static final int EDIT_EVENT = 12;

    public static final int DELETE_EVENT = 13;

    /**
     * @return the Class of the calendar this handler supports.
     */
    public Class getCalendarClass();

    /**
     * Returns the human-friendly name of the calendar type (e.g. 'iCalendar').
     * For display purposes only.
     * 
     * @return the display name of the calendar type.
     */
    public String getCalendarType();

    /**
     * Returns the XWork action name (within the namespace) for the specified
     * action type. If the calendar does not support the specified action,
     * <code>null</code> is returned.
     * 
     * @param calendar
     * @param action
     *            the action type (e.g. {@link #VIEW_CALENDAR},
     *            {@link #EDIT_EVENT})
     * @return The action name, or <code>null</code> if not supported.
     */
    public boolean supportsAction( ICalendar calendar, int action );

    /**
     * Returns the XWork namespace the actions for this calendar type are in.
     * 
     * @return the namespace.
     */
    public String getNamespace();
}
