/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.ical.action;

import com.atlassian.confluence.extra.calendar.ical.model.ICalCalendar;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.ICalendar;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.links.Link;
import com.opensymphony.util.TextUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;
import java.util.SortedMap;
import java.util.Collection;
import java.nio.charset.Charset;

import org.randombits.source.Source;
import org.randombits.source.StringSource;
import org.randombits.source.URLSource;
import org.randombits.source.confluence.AttachmentSource;

/**
 * Modifies the ICalendar instance.
 */
public class ModifyICalCalendarAction extends AbstractICalCalendarAction {
    private static final Pattern HASHED_PASSWORD = Pattern.compile( "\\*+" );

    private LinkResolver linkResolver;

    public ModifyICalCalendarAction() {
        super( true );
    }

    public String doAdd() {
        ICalCalendar calendar = new ICalCalendar();
        setSubCalendar( calendar );

        try {
            calendar.init();
        } catch ( CalendarException e ) {
            e.printStackTrace();
            addActionError( getText( "calendar.error.ical.calendarException", e.getMessage() ) );
            return ERROR;
        }

        String name = getName();
        if ( name == null || name.trim().length() == 0 ) {
            addActionError( getText( "calendar.error.ical.nameMissing" ) );
            return ERROR;
        }

        String result = updateCalendar( calendar, name );

        if ( result != null )
            return result;

        getCalendar().addChild( calendar );
        return saveCalendar();
    }

    private String updateCalendar( ICalCalendar calendar, String name ) {
        calendar.setName( name );

        if ( description != null && description.trim().length() > 0 )
            calendar.setDescription( description );
        else
            calendar.setDescription( null );

        if ( color != null && color.trim().length() > 0 )
            calendar.setColor( color );
        else
            calendar.setColor( null );

        return doSource( calendar );
    }

    private String doSource( ICalCalendar calendar ) {
        Source src = calendar.getSource();

        if ( LOCAL_SOURCE.equals( sourceType ) ) {
            if ( src == null )
                src = new StringSource();
            else if ( !( src instanceof StringSource ) ) {
                addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                return ERROR;
            }
        } else if ( REMOTE_SOURCE.equals( sourceType ) ) {
            URLSource urlSrc;
            if ( src == null ) {
                urlSrc = new URLSource();
                urlSrc.setConnectionTimeout( DEFAULT_TIMEOUT * 1000 );
                urlSrc.setReadTimeout( DEFAULT_TIMEOUT * 1000 );
            } else if ( src instanceof URLSource )
                urlSrc = ( URLSource ) src;
            else {
                addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                return ERROR;
            }

            if ( !TextUtils.stringSet( remoteSourceUrl ) ) {
                addActionError( getText( "calendar.error.ical.source.remoteUrlMissing" ) );
                return ERROR;
            }

            if ( remoteSourceUrl.startsWith( "webcal" ) )
                remoteSourceUrl = "http" + remoteSourceUrl.substring( 6 );          
            
            URL remoteSourceUrlObj = null;
            try {
                remoteSourceUrlObj = new URL ( remoteSourceUrl );
                urlSrc.setUrl( remoteSourceUrlObj );
            } catch ( MalformedURLException e ) {
                addActionError( e.getMessage() );
                return ERROR;
            }

            // CAL-247: don't allow a calendar to subscribe to an ical
            // calendar generated on the same Confluence page.
            Settings settings = settingsManager.getGlobalSettings();            
            if ( remoteSourceUrl.startsWith( settings.getBaseUrl() ) ) {
                // is the URL coming from the current page?
                if ( remoteSourceUrlObj.getPath().equals( getPage().getUrlPath() ) ) {
                    addActionError( getText( "calendar.error.ical.source.urlFromSamePage" ) );
                    return ERROR;        					                    
                }

                String urlQuery = remoteSourceUrlObj.getQuery();
                String[] queryParams = urlQuery.split( "&" );
                for ( int i = 0; i < queryParams.length; i++ ) {
                    if ( queryParams[i].startsWith( "pageId" ) ) {
                        String remotePageId = queryParams[i].substring( 7 );
                        if ( remotePageId.equals( String.valueOf( getPageId() ) ) ) {
                            addActionError( getText( "calendar.error.ical.source.urlFromSamePage" ) );
                            return ERROR;        					
                        }
                    }
                }
            }            
            
            try {
                int timeout = Integer.parseInt( connectionTimeout );
                urlSrc.setConnectionTimeout( timeout * 1000 );
                urlSrc.setReadTimeout( timeout * 1000 );
            } catch ( IllegalArgumentException iae ) {
                addActionError( iae.getMessage() );
                return ERROR;
            }

            urlSrc.setUsername( nonEmpty( remoteSourceUsername ) );
            if ( remoteSourcePassword == null || !HASHED_PASSWORD.matcher( remoteSourcePassword ).matches() )
                urlSrc.setPassword( nonEmpty( remoteSourcePassword ) );

            urlSrc.setEncoding( getRemoteSourceCharset() );

            src = urlSrc;
        }

        else if ( ATTACHMENT_SOURCE.equals( sourceType ) )

        {
            if ( TextUtils.stringSet( attachmentLink ) ) {
                if ( src == null ) {
                    AttachmentSource attSrc = new AttachmentSource();

                    Link link = linkResolver.createLink( getPage().toPageContext(), attachmentLink );
                    if ( link instanceof AttachmentLink ) {
                        Attachment att = ( ( AttachmentLink ) link ).getAttachment();
                        if ( !permissionManager.hasPermission( getRemoteUser(), Permission.VIEW, att ) ) {
                            addActionError( getText( "calendar.error.ical.source.attachmentLinkInaccessible" ) );
                            return ERROR;
                        }
                        attSrc.setContentId( att.getContent().getId() );
                        attSrc.setFileName( att.getFileName() );
                    } else {
                        addActionError( getText( "calendar.error.ical.source.attachmentLinkNotAttachment" ) );
                        return ERROR;
                    }
                    src = attSrc;
                } else if ( !( src instanceof AttachmentSource ) ) {
                    addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                    return ERROR;
                }
            } else {
                addActionError( getText( "calendar.error.ical.source.attachmentLinkMissing" ) );
                return ERROR;
            }
        }

        else

        {
            addActionError( getText( "calendar.error.ical.source.typeMissing" ) );
            return ERROR;
        }

        calendar.setSource( src );

        return null;
    }

    private Charset getRemoteSourceCharset() {
        String charsetName = nonEmpty( remoteSourceEncoding );
        if ( charsetName != null )
            return Charset.forName( charsetName );

        return null;
    }

    private String nonEmpty( String value ) {
        if ( value != null && value.trim().length() > 0 )
            return value;
        return null;
    }

    public String doEdit() {
        ICalCalendar calendar = ( ICalCalendar ) getSubCalendar();

        if ( calendar == null ) {
            addActionError( getText( "calendar.error.ical.calendarMissing" ) );
            return ERROR;
        }

        String name = getName();
        if ( name == null || name.trim().length() == 0 ) {
            addActionError( getText( "calendar.error.ical.nameMissing" ) );
            return ERROR;
        }

        String result = updateCalendar( calendar, name );
        if ( result != null )
            return result;

        return saveCalendar();
    }

    protected ICalendar createCalendar() {
        return new ICalCalendar();
    }

    public void setLinkResolver( LinkResolver linkResolver ) {
        this.linkResolver = linkResolver;
    }

    public Collection getAllCharsets() {
        SortedMap charsets = Charset.availableCharsets();

        return charsets.entrySet();
    }
}
