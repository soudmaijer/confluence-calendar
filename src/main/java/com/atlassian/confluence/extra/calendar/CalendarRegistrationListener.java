/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar;

import com.atlassian.event.Event;
import com.atlassian.event.EventListener;

import org.apache.log4j.Logger;

/**
 * An abstract base class for calendar extension plugins.
 */
public abstract class CalendarRegistrationListener implements EventListener {
    private Logger log = Logger.getLogger( getClass() );

    private boolean registered;

    /**
     * Implement this method to create a new CalendarHandler for alternate
     * calendar support.
     * 
     * @return The new calendar handler.
     */
    protected abstract CalendarHandler createCalendarHandler();

    /**
     * Handles events.
     * 
     * @param event
     *            The event to handle.
     */
    public synchronized void handleEvent( Event event ) {
        if ( !registered ) {
            CalendarManager calendarManager = CalendarManager.getInstance();
            if ( calendarManager != null ) {
                CalendarHandler handler = createCalendarHandler();
                calendarManager.registerHandler( handler );
                registered = true;
                log.info( "Registered calendar handler: " + handler.getCalendarType() );
            }
        }
    }

    /**
     * Returns a list of the event classes this handler handles.
     * 
     * @return The class list.
     */
    public Class[] getHandledEventClasses() {
        return new Class[0];
    }
}
