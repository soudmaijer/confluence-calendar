/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.model;

import java.util.List;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA. User: david Date: Nov 26, 2005 Time: 5:39:58 PM To
 * change this template use File | Settings | File Templates.
 */
public abstract class AParentCalendar extends ACalendar implements IParentCalendar {

    public List getDescendents() {
        List descendents = new java.util.LinkedList();

        List children = getChildren();

        if ( children != null ) {
            Iterator i = children.iterator();
            ICalendar child;

            while ( i.hasNext() ) {
                child = ( ICalendar ) i.next();
                descendents.add( child );

                if ( child instanceof IParentCalendar )
                    descendents.addAll( ( ( IParentCalendar ) child ).getDescendents() );
            }
        }

        return descendents;
    }

    private ICalendar findChild( String id, boolean descendents ) {
        if ( id == null )
            return null;

        ICalendar found = null;

        List children = getChildren();
        if ( children != null ) {
            Iterator i = children.iterator();
            ICalendar child;

            while ( i.hasNext() && found == null ) {
                child = ( ICalendar ) i.next();
                if ( id.equals( child.getId() ) )
                    found = child;
                else if ( descendents && child instanceof IParentCalendar )
                    found = ( ( IParentCalendar ) child ).getDescendent( id );
            }
        }

        return found;
    }

    public ICalendar getChild( String id ) {
        return findChild( id, false );
    }

    public ICalendar getDescendent( String id ) {
        return findChild( id, true );
    }

}
