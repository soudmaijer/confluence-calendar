package com.atlassian.confluence.extra.calendar.extractor;

import java.util.Iterator;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.joda.time.DateMidnight;
import org.joda.time.DateTimeUtils;
import org.joda.time.Interval;
import org.joda.time.Period;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.calendar.CalendarManager;
import com.atlassian.confluence.extra.calendar.ical.model.ICalEvent;
import com.atlassian.confluence.extra.calendar.model.CalendarGroup;
import com.atlassian.confluence.extra.calendar.model.ICalendar;

public class CalendarExtractor implements Extractor {

    public void addFields( Document document, StringBuffer sb, Searchable searchable ) {
        if ( !( searchable instanceof ContentEntityObject ) )
            return;

        ContentEntityObject object = ( ContentEntityObject ) searchable;
        String content = object.getBodyAsString();
        if ( content == null || content.length() == 0 )
            return;

        if ( content.length() > 10 && content.indexOf( ICalendarExtractor.CALENDAR_MARCO ) >= 0 ) {
            int begin = content.indexOf( ICalendarExtractor.CALENDAR_ID );
            int end = content.indexOf( ICalendarExtractor.CALENDAR_SEPARATOR );
            if ( end < 0 ) {
                end = content.length() - 1;
            }
            if ( begin >= 0 && end >= 0 ) {
                String ids = content.substring( begin, end );
                if ( ids == null || ids.length() == 0 )
                    return;
                String[] idArrays = ids.split( ICalendarExtractor.CALENDAR_EQUALS );
                if ( idArrays.length >= 2 ) {
                    String id = idArrays[1].trim();
                    CalendarGroup calendarGroup = ( CalendarGroup ) CalendarManager.getInstance().getCalendar(
                            object, id );
                    if ( calendarGroup == null )
                        return;

                    List calendars = calendarGroup.getDescendents();
                    if ( calendars != null && !calendars.isEmpty() )
                        indexCalendars( calendars, document, sb );

                    List events = calendarGroup.findEvents( new Interval( new DateMidnight( 1, 1, 1 ), Period
                            .years( 10000 ) ), DateTimeUtils.getZone( null ) );
                    if ( events != null && !events.isEmpty() )
                        indexCalendarEvents( events, document, sb );
                }
            }
        }
    }

    private void indexCalendars( List calendars, Document document, StringBuffer sb ) {
        Iterator iter = calendars.iterator();
        while ( iter.hasNext() ) {
            ICalendar iCalendar = ( ICalendar ) iter.next();
            if ( iCalendar != null ) {
                String name = iCalendar.getName();
                String desc = iCalendar.getDescription();
                String color = iCalendar.getColor();

                if ( name != null && name.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.CALENDAR_NAME, name, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( name ).append( ICalendarExtractor.SPACE );
                }

                if ( desc != null && desc.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.CALENDAR_DESC, desc, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( desc ).append( ICalendarExtractor.SPACE );
                }

                if ( color != null && color.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.CALENDAR_COLOUR, color, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( color ).append( ICalendarExtractor.SPACE );
                }
            }
        }
    }

    private void indexCalendarEvents( List events, Document document, StringBuffer sb ) {
        Iterator iter = events.iterator();
        while ( iter.hasNext() ) {
            ICalEvent event = ( ICalEvent ) iter.next();
            if ( event != null ) {
                String summary = event.getSummary();
                String location = event.getLocation();
                String description = event.getDescription();
                String link = event.getUrl() == null ? "" : event.getUrl().toASCIIString();

                if ( summary != null && summary.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.EVENT_SUMMARY, summary, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( summary ).append( ICalendarExtractor.SPACE );
                }

                if ( location != null && location.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.EVENT_LOCATION, location, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( location ).append( ICalendarExtractor.SPACE );
                }

                if ( description != null && description.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.EVENT_DESC, description, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( description ).append( ICalendarExtractor.SPACE );
                }

                if ( link != null && link.length() > 0 ) {
                    document.add( new Field( ICalendarExtractor.EVENT_LINK, link, Field.Store.NO,
                            Field.Index.TOKENIZED ) );
                    sb.append( link ).append( ICalendarExtractor.SPACE );
                }
            }
        }
    }
}
