package com.atlassian.confluence.extra.calendar.action;

import com.atlassian.confluence.core.Administrative;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.calendar.CalendarManager;

/**
 * View and update the license key for this Confluence installation
 */
public class CalendarAdminAction extends ConfluenceActionSupport implements Administrative {

    /**
     * Called to start the administration page
     * 
     * @return
     * @throws Exception
     */
    public String doUpdate() throws Exception {
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isOutlookCompatible() {
        return CalendarManager.getInstance().isOutlookCompatible();
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotOutlookCompatible() {
        CalendarManager.getInstance().setOutlookCompatible( false );
        return SUCCESS;
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsOutlookCompatible() {
        CalendarManager.getInstance().setOutlookCompatible( true );
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isNotesCompatible() {
        return CalendarManager.getInstance().isNotesCompatible();
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotNotesCompatible() {
        CalendarManager.getInstance().setNotesCompatible( false );
        return SUCCESS;
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsNotesCompatible() {
        CalendarManager.getInstance().setNotesCompatible( true );
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedUnfolding() {
        return CalendarManager.getInstance().isRelaxedUnfolding();
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedUnfolding() {
        CalendarManager.getInstance().setRelaxedUnfolding( false );
        return SUCCESS;
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedUnfolding() {
        CalendarManager.getInstance().setRelaxedUnfolding( true );
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedParsing() {
        return CalendarManager.getInstance().isRelaxedParsing();
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedParsing() {
        CalendarManager.getInstance().setRelaxedParsing( false );
        return SUCCESS;
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedParsing() {
        CalendarManager.getInstance().setRelaxedParsing( true );
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedValidation() {
        return CalendarManager.getInstance().isRelaxedValidation();
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedValidation() {
        CalendarManager.getInstance().setRelaxedValidation( false );
        return SUCCESS;
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedValidation() {
        CalendarManager.getInstance().setRelaxedValidation( true );
        return SUCCESS;
    }

}
