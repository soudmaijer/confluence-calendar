package com.atlassian.confluence.extra.calendar.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.FactoryBean;

public class ContentPermissionWrapperFactoryBean implements FactoryBean {

    private static final Logger logger = Logger.getLogger( ContentPermissionWrapperFactoryBean.class );

    public Object getObject() throws Exception {
        Object contentPermission;
        if ( null == ( contentPermission = getContentPermissionFromSecurityPackage() ) ) {
            if ( null == ( contentPermission = getContentPermissionFromCorePackage() ) ) {
                throw new IllegalStateException(
                        "Unable to find ContentPermission class from either com.atlassian.confluence.security or com.atlassian.confluence.core" );
            }
        }

        return new ContentPermissionWrapper( contentPermission );
    }

    protected Object getContentPermissionFromSecurityPackage() {
        try {
            final Class contentPermissionClass = Class
                    .forName( "com.atlassian.confluence.security.ContentPermission" );
            return contentPermissionClass.newInstance();
        } catch ( final Exception e ) {
            if ( logger.isDebugEnabled() )
                logger.debug(
                        "Unable to return an instance of com.atlassian.confluence.security.ContentPermission", e );
        }

        return null;
    }

    protected Object getContentPermissionFromCorePackage() {
        try {
            final Class contentPermissionClass = Class.forName( "com.atlassian.confluence.core.ContentPermission" );
            return contentPermissionClass.newInstance();
        } catch ( final Exception e ) {
            if ( logger.isDebugEnabled() )
                logger.debug(
                        "Unable to return an instance of com.atlassian.confluence.security.ContentPermission", e );
        }

        return null;
    }

    public Class getObjectType() {
        return ContentPermissionWrapper.class;
    }

    public boolean isSingleton() {
        return false;
    }
}
